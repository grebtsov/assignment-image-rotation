//
// Created by grebt on 28.10.2022.
//
#include "bmp_read.h"
#include "read_write.h"
#include <stdio.h>
/*
FILE * read(const char *file){
    return fopen(file, "rb");
}
FILE * write(const char *file){
    return fopen(file, "wb");
}
*/
FILE * open(const char *file, enum open_status status){
  if (status == R_B){
    return fopen(file, "rb");
  }
  else if (status == W_B){
    return fopen(file, "wb");
  }
  return 0;
}

int close(FILE* file){
    return fclose(file);
}

void free_im(struct image img){
    free(img.data);
}

void free_rt(struct image *rot){
    free(rot->data);
}
