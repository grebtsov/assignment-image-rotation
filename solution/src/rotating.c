//
// Created by grebt on 27.10.2022.
//
#include "bmp_read.h"

struct image *rotating (struct image *img) {
    struct image * rotated = malloc(sizeof (struct image));
    rotated->width = img->height;
    rotated->height = img->width;
    rotated->data = malloc(img->height * img->width * sizeof(struct pixel));
    for (uint64_t i = 0; i < img->width; i++) {
        for (uint64_t j = 0; j < img->height; j++) {
            rotated->data[i*rotated->width+j] = (img->data)[(rotated->width-j-1)*rotated->height+i];
        }
    }
    return rotated;
}

