//
// Created by grebt on 26.10.2022.
//
#include "bmp_read.h"
#include "new_head.h"
#include <inttypes.h>
#include <stdio.h>

uint64_t padding(uint64_t w){
    return (uint64_t)(w % 4);
}
enum read_status
from_bmp(FILE* in, struct image* image ) {
    struct bmp_header bmpHeader;
    if (fread(&bmpHeader, sizeof(struct bmp_header), 1, in) == 1){
        image = new_bmp(image, bmpHeader.biWidth, bmpHeader.biHeight);
        for (uint64_t i = 0; i < image->height; i++) {
            if ((fread(&(image->data[i * image->width]), sizeof(struct pixel), image->width, in)) !=image->width) {
                return READ_INVALID_ARRAY;
            }
            if ((fseek(in,(uint8_t) padding(image->width), SEEK_CUR)) != 0){
                return READ_INVALID_PADDING;
            }
        }
        return READ_OK;
    } else return READ_INVALID_HEADER;
}
//printf("\nread4");
/*
if(image->width % 4 == 0) {
    printf("\nread5/1");
    fread(&(image->data[i * image->width]), sizeof(struct pixel), image->width, in);
    printf("\nread5/2");
    fseek(in, (uint8_t) image->width % 4, SEEK_CUR);
    printf("\nread5/3");
}
if (image->width % 4 == 1) {
    printf("\nread6/1");
    fread(&(image->data[i * image->width]), sizeof(struct pixel), image->width, in);
    printf("\nread6/1");
    fseek(in, ((uint8_t) (image->width + 1) - (uint8_t) image->width) % 4, SEEK_CUR);
    printf("\nread6/1");
}
if (image->width % 4 == 2) {
    printf("\nread7/1");
    fread(&(image->data[i * image->width]), sizeof(struct pixel), image->width, in);
    printf("\nread7/1");
    fseek(in, ((uint8_t) (image->width + 2) - (uint8_t) image->width) % 4, SEEK_CUR);
    printf("\nread7/1");
}
if (image->width % 4 == 3) {
    printf("\nread8/1");
    fread(&(image->data[i * image->width]), sizeof(struct pixel), image->width, in);
    printf("\nread8/1");
    fseek(in, ((uint8_t) (image->width + 3) - (uint8_t) image->width) % 4, SEEK_CUR);
    printf("\nread8/1");
}*/

enum write_status
to_bmp (FILE* out, const struct image * image ) {
    struct bmp_header new_header = new_head(image);
    if (fwrite(&new_header, sizeof(struct bmp_header), 1, out) == 1) {
        const uint64_t f = 0;
        for (uint64_t i = 0; i < image->height; i++) {
            if ((fwrite(&(image->data[i * image->width]), sizeof(struct pixel), image->width, out)) != image->width){
                return WRITE_INVALID_ARRAY;
            }
            if((fwrite(&f, 1, padding(image->width), out)) != padding(image->width)) {
                return WRITE_INVALID_PADDING;
            }
        }
        return WRITE_OK;
    } else return WRITE_INVALID_HEADER;
}
