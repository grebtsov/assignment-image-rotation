//
// Created by grebt on 27.10.2022.
//
#include "bmp_read.h"
#include <inttypes.h>

struct image * new_bmp(struct image * image, uint64_t width, uint64_t height){
    image->width = width;
    image->height = height;
    image->data = malloc(image->height * image->width * sizeof(struct pixel));
    //printf("%" PRIi64  "\n", image->height * image->width * sizeof(struct pixel));
    return image;
}


