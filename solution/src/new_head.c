//
// Created by grebt on 27.10.2022.
//
#include "bmp_read.h"

struct bmp_header new_head(const struct image* image) {
    struct bmp_header bmp_header = {
            .bfType = 0x4D42,
            .bfileSize = image->height * image->width * sizeof(struct pixel) + image->height * padding(image->width) * sizeof(struct pixel),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image->height * image->width * sizeof(struct pixel) + padding(image->width) * image->height,
            .biXPelsPerMeter = 2834,
            .biYPelsPerMeter = 2834,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    return bmp_header;
}
