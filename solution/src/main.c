#include "bmp_read.h"
#include "read_write.h"
#include "rotating.h"
#include <stdio.h>

int main(int argc, char** argv) {
    (void) argc; (void) argv;
    /*if (argc >= 3) {
        printf("Problems\n");
        return 1;
    }*/
    FILE *f = open(argv[1], R_B);
    FILE *f1 = open(argv[2], W_B);
    struct image image;
    if(from_bmp((FILE *) f, &image) == READ_OK){

      //printf("%" PRIi64 "\n", image->width);
      //printf("%" PRIi64 "\n", image->height);
      //printf("%" PRIi64  "\n", bmpHeader.biSizeImage);
      //from_bmp((FILE *) f, &image);
      struct image *rotated = rotating(&image);
      //to_bmp(f1, image);
      if(to_bmp(f1, rotated) == WRITE_OK){
        free_im(image);
        free_rt(rotated);
        close(f);
        close(f1);
        return 0;
      }
      else{
        free_im(image);
        free_rt(rotated);
        return 1;
      }
    }
    else{
      free_im(image);
      return 1;
    }
}


/*
int main() {
    struct bmp_header bmpHeader;
    char* path;
    path = "C:\\Users\\grebt\\CLionProjects\\untitled1\\input.bmp";
    FILE *file = fopen(path, "rb");


    char* path1;
    path1 = "C:\\Users\\grebt\\CLionProjects\\untitled1\\output.bmp";
    FILE *file1;
    file1 = fopen(path1, "wb");
    if(file1 == NULL) return 6;

    struct image * image = malloc(sizeof (struct image));
    from_bmp((FILE *) file, image);
    printf("%" PRIi64 "\n", image->width);
    printf("%" PRIi64 "\n", image->height);
    //printf("%" PRIi64  "\n", bmpHeader.biSizeImage);
    struct image *rotated = rotating(image);
    //to_bmp(file1, image);
    to_bmp(file1, rotated);
}
*/
