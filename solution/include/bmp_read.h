//
// Created by grebt on 26.10.2022.
//
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#ifndef UNTITLED1_BMP_READ_H
#define UNTITLED1_BMP_READ_H
struct pixel { uint8_t b, g, r; };

#pragma pack(push, 1)

struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

enum read_status  {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_INVALID_ARRAY,
    READ_INVALID_PADDING
};

enum read_status from_bmp(FILE* in, struct image* image);

enum  write_status  {
    WRITE_OK = 0,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_ARRAY,
    WRITE_INVALID_PADDING
};

enum write_status to_bmp(FILE* out, const struct image* image);

struct image * new_bmp(struct image * image, uint64_t width, uint64_t height);
uint64_t padding(uint64_t w);

#endif //UNTITLED1_BMP_READ_H

