//
// Created by grebt on 26.10.2022.
//
#include <stdio.h>
#ifndef UNTITLED1_READ_WRITE_H
#define UNTITLED1_READ_WRITE_H

#pragma once
//FILE * read(const char *file);
//FILE * write(const char *file);

enum open_status{
    R_B,
    W_B
};

int close(FILE* file);
void free_im(struct image img);
void free_rt(struct image *rot);
FILE * open(const char *file, enum open_status status);
#endif //UNTITLED1_READ_WRITE_H

